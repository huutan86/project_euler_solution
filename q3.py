import math
from absl import logging
def _is_prime(x: int) -> bool:
    if x == 2:
        return True

    if x == 1:
        return False

    for y in range(2, int(math.sqrt(x)) + 1):
        if x % y == 0:
            return False

    return True

def q3():
    """The prime factors of 13195 are 5, 7, 13 and 29.
    What is the largest prime factor of the number 600851475143 ?"""
    max_factor = 1
    num = 600851475143
    all_factors = []
    for x in range(2, int(math.sqrt(num))):
        if num % x == 0 and _is_prime(x):
            all_factors.append(x)
    logging.info(f"res = {max(all_factors)}")