from absl import logging
def q6() -> None:
    sum_squared = sum(range(101)) ** 2
    square_sum = sum(x ** 2 for x in range(101))
    logging.info(f"Res = {sum_squared - square_sum}")