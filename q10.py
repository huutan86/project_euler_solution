import math
from absl import logging

def _is_prime(x: int) -> bool:
    if x == 2:
        return True

    if x == 1:
        return False

    for y in range(2, int(math.sqrt(x)) + 1):
        if x % y == 0:
            return False

    return True
def q10():
    all_primes = [x for x in range(1, 2000001) if _is_prime(x)]
    logging.info(f"res = {sum(all_primes)}")