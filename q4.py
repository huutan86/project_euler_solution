from absl import logging

from itertools import product
def _is_palindrome(x: int) -> bool:
    if str(x) == str(x)[::-1]:
        return True
    return False

def q4():
    """A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit
    numbers is 9009 = 91 × 99.
    Find the largest palindrome made from the product of two 3-digit numbers."""
    all_palindrome = []
    for x, y in product(range(1000), range(1000)):
        if _is_palindrome(x * y):
            all_palindrome.append(x * y)
    logging.info(f"res = {max(all_palindrome)}")