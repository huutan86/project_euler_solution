# Source code to run the solution for all days.
import importlib
from pathlib import Path
from glob import glob

from absl import app
from absl import flags
from absl import logging

flags.DEFINE_integer("question", None, "The 1-based index of the day to solve the challenge.")
flags.mark_flag_as_required("question")
FLAGS = flags.FLAGS


def _import_modules_of_all_questions():
    all_questions = glob(str(Path(__file__).parent / "q*.py"))
    all_days = [Path(x).name for x in all_questions]
    #questions = re.findall("q*.py", all_days)
    questions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    for q in questions:
        module_name = f"q" \
                      f"{q}"
        if Path(f"./{module_name}.py").exists():
            globals()[module_name] = getattr(importlib.import_module(module_name), module_name)
            logging.info(f"Imported day {q} module.")


def main(argv) -> None:
    del argv
    logging.info(f"Solving challenge of day {FLAGS.question}")

    try:
        globals()[f"q{FLAGS.question}"]()
    except Exception as e:
        raise ValueError(f"Can't call the function on question {FLAGS.question}")
        raise e


_import_modules_of_all_questions()
if __name__ == "__main__":
    app.run(main)