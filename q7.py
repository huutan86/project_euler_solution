import logging
import math

def _is_prime(x: int) -> bool:
    if x == 2:
        return True

    if x == 1:
        return False

    for y in range(2, int(math.sqrt(x)) + 1):
        if x % y == 0:
            return False

    return True

def q7() -> None:
    count = 0
    cur_val = 1
    idx_to_find = 10001
    while True:
        if _is_prime(cur_val):
            count += 1
            if count == idx_to_find:
                logging.info(f"Prime num = {cur_val}")
                break
        cur_val += 1
