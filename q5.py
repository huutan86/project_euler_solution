from absl import logging
from typing import List
from operator import mul
from functools import reduce
from collections import Counter

import math

def _is_prime(x: int) -> bool:
    if x == 2:
        return True

    if x == 1:
        return False

    for y in range(2, int(math.sqrt(x)) + 1):
        if x % y == 0:
            return False

    return True

def factorize(x: int) -> List[int]:
    fact_res = []
    res = x
    if x == 1:
        fact_res = [1]
    else:
        while res > 1:
            if _is_prime(res):
                fact_res.append(res)
                break

            for y in range(1, int(math.sqrt(res)) + 1):
                if _is_prime(y) and res % y == 0:
                    fact_res.append(y)
                    res = res // y
                    break

    return fact_res

def q5():
    """2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
    What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?"""
    factorize(4)
    all_factors = [Counter(factorize(x)) for x in range(1, 20)]
    all_primes = set(x for y in all_factors for x in y)
    prime_max_count = {x: max(y.get(x, 0) for y in all_factors) for x in all_primes}
    temp = [x ** y for x, y in prime_max_count.items()]
    logging.info(f"res = {reduce(mul, temp)}")
